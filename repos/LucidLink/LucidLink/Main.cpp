#include "mongoose.h";
#include <string>
#include <iostream>

static struct mg_serve_http_opts s_http_server_opts;
//Event handler
static void ev_handler(struct mg_connection* nc, int ev, void *p) {
	if (ev == MG_EV_HTTP_REQUEST) {
		//Serve static html files
		mg_serve_http(nc, (struct http_message*)p, s_http_server_opts);
	}
}
int initServer(int port) {
	//Mongoose event manager
	struct mg_mgr mgr;
	//Mongoose connection
	struct mg_connection* nc;
	//Conver port variable to char
	std::string portTochar = std::to_string(port);
	static char const* sPort = portTochar.c_str();
	//Initialize mongoose
	mg_mgr_init(&mgr, NULL);
	std::cout << "Starting web server on port " << sPort << std::endl;
	nc = mg_bind(&mgr, sPort, ev_handler);
	// If the connection fails
	if (nc == NULL) {
		std::cout << "Failed to create listener" << std::endl;
		return 1;
	}
	//Set up HTTP server options
	mg_set_protocol_http_websocket(nc);

	s_http_server_opts.document_root = ".";
	s_http_server_opts.enable_directory_listing = "yes";
	for (;;) {
		mg_mgr_poll(&mgr, 1000);
	}
	//Free up all memory allocated
	mg_mgr_free(&mgr);
	return 0;
}

int main(void) {
	int port;
	std::cout << "Select server port" << std::endl;
	std::cin >> port;
	//fail case
	if (std::cin.fail()) {
		port = 1000;
	}
	initServer(port);
	return 0;
}
